# -*- coding: utf-8 -*-
import scrapy

import cartypeyearitems
import json

class CarTypeYearsSpider(scrapy.Spider):
    name = "cartypeyears"

    def start_requests(self):
        urls=[
            'http://jy.ycqpmall.com/ajax.php'
        ]
        for url in urls:
            # FormRequest 是Scrapy发送POST请求的方法
            with open('data/jiyou/jiyou_cars2.json') as file:
                carsjons=json.load(file)
                for jsonitem in carsjons:
                    print jsonitem;
                    if(jsonitem['cartypeid']!='0'):
                        request= scrapy.FormRequest(
                            url=url,
                            formdata={"action": "category", "category_title": "请选择",'category_moduleid':'16','category_extend:':'','category_deep':'0','cat_id':'1','catid':''+jsonitem['cartypeid']+''},
                            callback=self.parse
                        )
                        request.meta['cartypeid']= jsonitem['cartypeid']
                        yield  request

    def parse(self, response):
        se=response.selector.xpath('//select')[2]
        for option  in se.xpath('.//option'):
            item = cartypeyearitems.CarTypeYearItem()
            item['cartypeyearname']=option.xpath('text()').extract_first()
            item['cartypeyearid'] =option.xpath('@value').extract_first()
            item['cartypeid'] = response.meta['cartypeid']
            yield  item



