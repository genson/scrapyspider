# -*- coding: utf-8 -*-
import scrapy

import cartypeitems
import json

class CarTypesSpider(scrapy.Spider):
    name = "cartypes"

    def start_requests(self):
        urls=[
            'http://jy.ycqpmall.com/ajax.php' #机油 第二个下拉框
        ]
        for url in urls:
            # FormRequest 是Scrapy发送POST请求的方法
            with open('data/jiyou/jiyou_cars.json') as file:
                carsjons=json.load(file)
                for jsonitem in carsjons:
                    print 'json record:'
                    print jsonitem;
                    request= scrapy.FormRequest(
                        url=url,
                        formdata={"action": "category", "category_title": "请选择",'category_moduleid':'16','category_extend:':'','category_deep':'0','cat_id':'0','catid':''+jsonitem['carid']+''},
                        callback=self.parse
                    )
                    request.meta['carid']= jsonitem['carid']
                    yield  request

    def parse(self, response):


        se=response.selector.xpath('//select')[1]

        for option  in se.xpath('.//option'):
            caritem = cartypeitems.CarTypeItem()
            caritem['cartypename']=option.xpath('text()').extract_first()
            caritem['cartypeid'] =option.xpath('@value').extract_first()
            caritem['carid'] = response.meta['carid']
            caritem['type']='jiyou-2'
            yield  caritem



