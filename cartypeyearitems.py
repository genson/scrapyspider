# -*- coding: utf-8 -*-
import  scrapy
from scrapy.item import Item,Field

class CarTypeYearItem(scrapy.Item):
    cartypeyearname= Field()
    cartypeyearid= Field()
    cartypeid= Field()