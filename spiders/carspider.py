# -*- coding: utf-8 -*-
import scrapy

import caritems


class CarSpider(scrapy.Spider):
    name = "cars"

    def start_requests(self):
        urls=[
            'http://jy.ycqpmall.com/mall/search.php?list=0&catid=1067&x=45&y=16' #机油
        ]
        for url in urls:
            yield scrapy.Request(url=url,callback=self.parse)

    def parse(self, response):


        se=response.selector.xpath('//select')[0].xpath('.//option')
        for option  in se:
            caritem = caritems.CarItem()
            caritem['carname']=option.xpath('text()').extract_first()
            caritem['carid'] =option.xpath('@value').extract_first()
            caritem['type']='jiyou'
            yield  caritem



