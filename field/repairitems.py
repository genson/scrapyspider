# -*- coding: utf-8 -*-
import  scrapy
from scrapy.item import Item,Field

class RepairItem(scrapy.Item):
    group= Field()
    itemtypelink= Field()
    itemtype= Field()
