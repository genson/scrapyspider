# -*- coding: utf-8 -*-
import  scrapy
from scrapy.item import Item,Field

class CarTypeItem(scrapy.Item):
    cartypename= Field()
    cartypeid= Field()
    carid= Field()
    type=Field()